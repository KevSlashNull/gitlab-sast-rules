#!/usr/bin/env bash

set -e

# Grab latest semgrep
git clone https://gitlab.com/gitlab-org/security-products/analyzers/semgrep.git

# sast-rules.zip is an artifact built in verify stage
cp sast-rules.zip semgrep/
cd semgrep

# Inject copy of our sast-rules.zip
sed -i -r 's/(\/analyzer-binary)/\1\nCOPY sast-rules.zip \/sast-rules.zip/;' Dockerfile
# Noop out getting latest rules since we copied in our sast-rules
sed -i 's/wget https:\/\/gitlab.com\/api\/v4\/projects\/${SAST_RULES_PROJECTID}\/packages\/generic\/sast-rules\/v${SAST_RULES_VERSION}\/sast-rules.zip/\/bin\/true/g' Dockerfile

# Noop out VET and private registries that will fail building
sed -i 's/FROM registry.gitlab.com\/security-products\/vet:${VET_VERSION} AS vet//g' Dockerfile
sed -i 's/FROM registry.gitlab.com\/gitlab-org\/security-products\/vet\/stencils:${STENCILS_VERSION} AS recipes//g' Dockerfile
sed -i 's/COPY --from=vet \/usr\/bin\/analyzer \/vet//g' Dockerfile
sed -i 's/COPY --from=recipes \/config\/verify \/verify//g' Dockerfile

# Now build out our docker image with _this_ branch's updated rules instead of allowing the Dockerfile to pull latest rules
docker build -t semgrep-sast-rules -f Dockerfile .
docker info
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
docker tag semgrep-sast-rules registry.gitlab.com/gitlab-org/security-products/sast-rules/semgrep-sast-rules:$TARGET_TAG
docker push registry.gitlab.com/gitlab-org/security-products/sast-rules/semgrep-sast-rules:$TARGET_TAG

# Move back to top level dir
cd ..

# prepare BAP related information
echo "UPSTREAM_JOB_ID=$CI_JOB_ID" >> build.env
cp qa/bap/work.json work.json
# Get latest semgrep
export SOURCE_TAG=$(curl -sS -L "https://gitlab.com/api/v4/projects/${SEMGREP_PROJECT_ID}/releases/permalink/latest" | jq '.name' | tr -d 'v\"')
# update the work.json SOURCE_TAG to point to latest semgrep container
sed -i s/SOURCE_TAG/$SOURCE_TAG/g work.json
# update work.json TARGET_TAG to point to our newly created semgrep-sast-rules:$TARGET_TAG
sed -i s/TARGET_TAG/$TARGET_TAG/g work.json
