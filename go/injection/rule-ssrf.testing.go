// License: MIT

package main_test

import (
	"net/http"
	"os"
	"testing"
)

func TestSSRF(t *testing.T) {
	u := os.Getenv("URL")
	// cannot use Semgrep assertion here - as it triggers a rule id mismatch
	http.Get(u)
}
