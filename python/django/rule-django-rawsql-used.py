# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/django_sql_injection_raw.py
# hash:  8eee173

from django.db.models.expressions import RawSQL
from django.contrib.auth.models import User

# ruleid: python_django_rule-django-rawsql-used
User.objects.annotate(val=RawSQL('secure', []))
# ruleid: python_django_rule-django-rawsql-used
User.objects.annotate(val=RawSQL('%secure' % 'nos', []))
# ruleid: python_django_rule-django-rawsql-used
User.objects.annotate(val=RawSQL('{}secure'.format('no'), []))
raw = '"username") AS "val" FROM "auth_user" WHERE "username"="admin" --'
# ruleid: python_django_rule-django-rawsql-used
User.objects.annotate(val=RawSQL(raw, []))
raw = '"username") AS "val" FROM "auth_user"' \
      ' WHERE "username"="admin" OR 1=%s --'
# ruleid: python_django_rule-django-rawsql-used
User.objects.annotate(val=RawSQL(raw, [0]))
