// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/file/FileDisclosure.java
// hash: a7694d0

package injection;

import java.io.IOException;
import org.apache.struts.action.ActionForward;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

// REQUESTDISPATCHER_FILE_DISCLOSURE
public class FileDisclosure extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            String returnURL = request.getParameter("returnURL");

            /******Struts ActionForward vulnerable code tests******/
            // ruleid: java_inject_rule-FileDisclosure
            ActionForward forward = new ActionForward(returnURL); //BAD

            // ruleid: java_inject_rule-FileDisclosure
            ActionForward forward2 = new ActionForward(returnURL, true); //BAD

            // ruleid: java_inject_rule-FileDisclosure
            ActionForward forward3 = new ActionForward("name", returnURL, true); //BAD

            // ruleid: java_inject_rule-FileDisclosure
            ActionForward forward4 = new ActionForward("name", returnURL, true); //BAD

            ActionForward forward5 = new ActionForward();
            // ruleid: java_inject_rule-FileDisclosure
            forward5.setPath(returnURL); //BAD

            //false positive test - returnURL moved from path to name (safe argument)
            ActionForward forward6 = new ActionForward(returnURL, "path", true); //OK

            /******Spring ModelAndView vulnerable code tests******/
            // ruleid: java_inject_rule-FileDisclosure
            ModelAndView mv = new ModelAndView(returnURL); //BAD

            // ruleid: java_inject_rule-FileDisclosure
            ModelAndView mv2 = new ModelAndView(returnURL, new HashMap()); //BAD

            // ruleid: java_inject_rule-FileDisclosure
            ModelAndView mv3 = new ModelAndView(returnURL, "modelName", new Object()); //BAD

            ModelAndView mv4 = new ModelAndView();
            // ruleid: java_inject_rule-FileDisclosure
            mv4.setViewName(returnURL); //BAD

            //false positive test - returnURL moved from viewName to modelName (safe argument)
            ModelAndView mv5 = new ModelAndView("viewName", returnURL, new Object()); //OK

        }catch(Exception e){
            System.out.println(e);
        }
    }

    public void doGet2(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            String jspFile = request.getParameter("jspFile");

            RequestDispatcher requestDispatcher = request.getRequestDispatcher(jspFile);

            // ruleid: java_inject_rule-FileDisclosure
            requestDispatcher.include(request, response);

            requestDispatcher = request.getSession().getServletContext().getRequestDispatcher(jspFile);

            // ruleid: java_inject_rule-FileDisclosure
            requestDispatcher.forward(request, response);

        }catch(Exception e){
            System.out.println(e);
        }
    }
}
