// License: LGPL-3.0 License (c) find-sec-bugs
// scaffold: dependencies=com.amazonaws.aws-java-sdk-simpledb@1.12.187
package inject;
import java.io.IOException;
import java.util.Arrays;

public class CommandInjection {

     public void danger(String cmd) throws IOException {
        Runtime r = Runtime.getRuntime();
        // ruleid: java_inject_rule-CommandInjection
        String[] cmds = new String[] {
            "/bin/sh",
            "-c",
            cmd
        };

        // ruleid: java_inject_rule-CommandInjection
        r.exec(cmd);
        r.exec(new String[]{"test"});
        // ruleid: java_inject_rule-CommandInjection
        r.exec(new String[]{"bash", cmd},new String[]{});
        // ruleid: java_inject_rule-CommandInjection
        r.exec(new String[]{"bash"+ cmd},new String[]{});

        String tainted = "bash"+ cmd + "test";
        r.exec(tainted);
        r.exec(tainted + "custom");
        r.exec(new String[]{"bash", tainted},new String[]{});
        r.exec(new String[]{"/bin/sh", "-c" + tainted},new String[]{});

        r.exec(cmds);
        r.exec(cmds,new String[]{});
        r.exec(cmds,new String[]{"test"});
    }

    public void danger2(String cmd) {
        ProcessBuilder b = new ProcessBuilder();
        // ruleid: java_inject_rule-CommandInjection
        b.command(cmd);
        b.command("test");
        // ruleid: java_inject_rule-CommandInjection
        b.command(Arrays.asList("/bin/sh", "-c", cmd));

        String tainted = "test2"+ cmd + "test";
        // ruleid: java_inject_rule-CommandInjection
        b.command("test2"+ cmd + "test");
        // ruleid: java_inject_rule-CommandInjection
        b.command(tainted);
        // ruleid: java_inject_rule-CommandInjection
        b.command(Arrays.asList("/bin/sh", "-c", tainted));
    }
}
