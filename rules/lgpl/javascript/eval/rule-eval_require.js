// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/ajinabraham/njsscan/blob/master/tests/assets/node_source/true_positives/semantic_grep/eval/eval_require.js
// hash: e7a0a61
const express = require('express')
const app = express()
const port = 3000

const hardcodedPath = 'lib/func.js'

function testController1(req, res) {
    try {
        // ruleid: rules_lgpl_javascript_eval_rule-eval-require
        require(req.query.controllerFullPath)(req, res);
    } catch (err) {
        this.log.error(err);
    }
    res.end('ok')
};
app.get('/test1', testController1)

let testController2 = function (req, res) {
    // ruleid: rules_lgpl_javascript_eval_rule-eval-require
    const func = require(req.body)
    return res.send(func())
}
app.get('/test2', testController2)

var testController3 = null;
testController3 = function (req, res) {
    // ruleid: rules_lgpl_javascript_eval_rule-eval-require
    const func = require(req.body)
    return res.send(func())
}
app.get('/test3', testController3)

    (function (req, res) {
        // ruleid: rules_lgpl_javascript_eval_rule-eval-require
        const func = require(req.body)
        return res.send(func())
    })(req, res)

