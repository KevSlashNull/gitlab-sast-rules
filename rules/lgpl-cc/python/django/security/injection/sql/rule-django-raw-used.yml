# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
  - id: python_django_rule-django-raw-used
    message: |
      SQL Injections are a critical type of vulnerability that can lead to data 
      or system compromise. By dynamically generating SQL query strings, user 
      input may be able to influence the logic of the SQL statement. 
      This could lead to an adversary accessing information they should not 
      have access to, or in some circumstances, being able to execute OS functionality
      or code.

      Replace all dynamically generated SQL queries with parameterized queries. 
      In situations where dynamic queries must be created, never use direct user input,
      but instead use a map or dictionary of valid values and resolve them using a user 
      supplied key.

      For example, some database drivers do not allow parameterized queries for 
      `>` or `<` comparison operators. In these cases, do not use a user supplied 
      `>` or `<` value, but rather have the user supply a `gt` or `lt` value. 
      The alphabetical values are then used to look up the `>` and `<` values to be used 
      in the construction of the dynamic query. The same goes for other queries where 
      column or table names are required but cannot be parameterized.

      Data that is possible user-controlled from a python request is passed
      to `raw()` function. To remediate this issue, use django's QuerySets, 
      which are built with query parameterization and therefore not vulnerable 
      to sql injection. For example, you could use `Entry.objects.filter(date=2006)`
      
      If for some reason this is not feasible, ensure calls including user-supplied 
      data pass it in to the `params` parameter of the `raw()` method.
      Below is an example using `raw()`, passing in user-supplied data as `params`. 
      This will treat the query as a parameterized query and `params` as strictly data, 
      preventing any possibility of SQL Injection.

      ```
      def test(request):
        uname = request.GET["username"] 
        res = User.objects.raw('SELECT * FROM myapp_user WHERE username = %s', (uname,))
      ```

      For more information on QuerySet see:
      - https://docs.djangoproject.com/en/5.0/ref/models/querysets/

      For more information on SQL Injection see OWASP:
      - https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html

    metadata:
      cwe: "CWE-89"
      owasp: "A1:2017-Injection"
      category: "security"
      shortDescription: "Improper neutralization of special elements used in an SQL Command ('SQL Injection')"
    languages:
      - python
    severity: ERROR
    security-severity: High
    mode: taint
    pattern-sources:
      - patterns:
          - pattern: $PARAM
          - pattern-inside: |
              def $VIEW(...,$PARAM,...):
                ...
                return ...    
    pattern-sinks:
      - patterns:
          - pattern: $QUERY
          - pattern-inside: $MODEL.objects.raw($QUERY, ... )

        

