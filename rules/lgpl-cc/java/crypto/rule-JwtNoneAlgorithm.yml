# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://semgrep.dev/r?q=java.jjwt.security.jwt-none-alg.jjwt-none-alg
# yamllint enable
---
rules:
  - id: java_crypto_rule_JwtNoneAlgorithm
    message: |
      Detected use of the 'none' algorithm in a JWT token. The 'none'
      algorithm assumes the integrity of the token has already been verified.
      This would allow a malicious actor to forge a JWT token that will
      automatically be verified. Do not explicitly use the 'none' algorithm.
      Instead, use an algorithm such as 'HS256'.

      For more information on how to securely use JWT please see OWASP:
      - https://cheatsheetseries.owasp.org/cheatsheets/JSON_Web_Token_for_Java_Cheat_Sheet.html
    metadata:
      shortDescription: "Use of a broken or risky cryptographic algorithm"
      cwe: "CWE-327"
      owasp:
        - "A03:2017-Sensitive Data Exposure"
        - "A02:2021-Cryptographic Failures"
      source-rule-url: https://semgrep.dev/blog/2020/hardcoded-secrets-unverified-tokens-and-other-common-jwt-mistakes/
      asvs:
        section: "V3: Session Management Verification Requirements"
        control_id: 3.5.3 Insecue Stateless Session Tokens
        control_url: https://github.com/OWASP/ASVS/blob/master/4.0/en/0x12-V3-Session-management.md#v35-token-based-session-management
        version: "4"
      category: security
      technology:
        - jwt
      confidence: LOW
      references:
        - https://owasp.org/Top10/A02_2021-Cryptographic_Failures
      subcategory:
        - audit
      likelihood: LOW
      impact: MEDIUM
      license: Commons Clause License Condition v1.0[LGPL-2.1-only]
      vulnerability_class:
        - Cryptographic Issues
      security-severity: "CRITICAL"
    languages:
      - java
    severity: ERROR
    pattern-either:
      - patterns:
          - pattern: |
              io.jsonwebtoken.Jwts.builder()
          - pattern-not-inside: |
              $RETURNTYPE $FUNC(...) {
                ...
                $JWTS.signWith(...);
                ...
              }
      - pattern: |
          $J.sign(com.auth0.jwt.algorithms.Algorithm.none())
      - pattern: |
          new com.nimbusds.jose.PlainHeader(...);
      - pattern: |
          new com.nimbusds.jose.PlainHeader.Builder(). ... .build();
